**Git Exercise**

# Version Control Exercise
Main purpose of this exercise is that students strengthen their knowledge of using a Source/Version Control system, in this case Git. Make sure you have Git installed on your PC:

	git --version

You should see something like:

    git version 2.6.2

If you don´t get similar output; you must install Git on the machine. There is a great chapter on that subject in the [Pro Git Book](http://git-scm.com/book/en/Getting-Started-Installing-Git), but for our Advania Qstack team server you should be able to figure this out after last weeks exercises (right?).

## Pro Git Book
Yes! There is a fantastic book online, free of charge, on Git. The book is titled [Pro Git book](http://git-scm.com/book), written by Scott Chacon and published by Apress. We recommend that students skim through the first chapters, and when you have any Git troubles, this book has the answers. The first two chapters and the next 30 pages or so should get you going.

## Version controlEach team needs to have a version control server, we recommend that you use some free service e.g. from [GitHub](https://github.com/) or [Bitbucket](http://bitbucket.org). These services provide Git based projects. We have given a lecture on how to create an organization and repositories using Github so we recommend that students use Github when solving this exercise and other exercises in this course.
## ExerciseThe team will create a shared list of their favorite movies and try out the basic source control commands while doing it. These instructions are written for Github. If you decide to use another service then you must go through the instructions they provide on creating organizations and repositories. If in doubt, use Github.
### Setting up a repositoryFirst, each team member must sign-up for a Github account. If you already have an account you can use that one.

Your team should create an Organization for the repositories. This organization is a hypothetical company where you and your team members are employees working on some cool projects. Each repository under this organization are products that your company is producing. Yes, in this exercise you own a company. This step is optional for this exercise but when you continue on other projects in this course it will be required that the repositories that the group owns is within an organization. So we recommend that your group creates organization now. This can be done from [the organization page](https://github.com/account/organizations/new).

With that ready, one of the team member can create a new repository for this exercise. This can be done from https://github.com/organizations/*organization-name*/repositories/new. Name the repository with some descriptive name, such as *GitExerciseInClass*.

**Note**: When you are creating the repository, Git offers you to add initial *Readme* file to the repository. We recommend that you allow Github to create this file for you where this creates the initial commit in your repository. This saves you the effort of creating that commit yourself. If not, you must create the remote branch yourself.
### Adding filesOne member of the team does the following:Clone the newly created repository to your local drive. On the repository page on Github you can find a clone link that you can copy and use. Open up your terminal, change to some good location, such as "/home/username" and type in the following lines:
	git clone git@github.com:Organization-name/repository-name.gitThen create a file called *movies.txt* and add one item to the list e.g. "The Matrix" (the best movie ever made!). Add the file under version control using `git add` as follows.
	git add movies.txt
Commit the changes with `git commit`. Add 2-3 items to the list and commit file after addition of each item. Remember to add descriptive comments with your changes.
Use `git log` and `git status` to monitor the changes that your are doing.
When you are happy with the changes that you have added, then push the index to Github (using `git push`) so other group members can pull the changes (using `git pull`) to their local clones.### Work on the projectEach member of the team does all the following steps:1. Get a working clone to your computer2. Find and change the file put in your own items in the list, communicate within the group so you don ́t do it all in the same line3. Commit your change (with comments) Note: You ́ll have to do an pull before committing if someone beat you to committing4. Examine what happened to the file. What happened?
### ConflictEach member of the team does the following:1. Choose one item on the list and everybody change to something new2. Commit and update as needed. Someone at least should get a conflict.3. Resolve the conflict.
There are multiple ways and various tools that you can use when resolving conflicts.

If you don't have any luxury of third party GUI merging tools (this is often the case when you are working on a terminal remotely) you can always resolve conflicts manually by hand. We recommend you resolve the conflict by hand in this exercise.

This is done by editing the file that is in a conflict state, remove the conflict lines within the document, add the file to index and commit the changes. [The Pro Git book](http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging#Basic-Merge-Conflicts) has a short chapter on resolving merge conflicts that is good to read when solving this part of the exercise.### BranchingCreate a branch for yourself. F.ex. master-username. That is, you have a branch named after you. This can be done as follows.
	git checkout -b master-username`git status` or `git branch` shows you what branch your are currently working on. Change the file in your branch. Add a comment. When you push your local branch
to Github up you have to do it as follows.

	git push -u origin master-username

Note that you only have to do this the first time you "publish" your branch.
After that you can do commit and push to the branch.

To see what branch are available or what branch is currently selected, use `git branch`. To change to a branch that has already been created, we use the git checkout command as follows.

	git checkout branch-name### MergingMerge the changes that you have added to your user branch into the master branch. If you do `git status` you should see that you are currently on your special branch. The goal is to merge the changes that you have in this branch to the master branch. Move over to the master branch, do a `git pull` and merge the branch using `git merge` as follows.
	git merge master-username
That is, you are merging the master-username branch into the branch that is currently active. In our case, that should be the master branch.
### Reverting
Do a random change in your branch. -Oh man, this was not a wise change. This happens when you are coding and drinking beer at the same time.

In Git it depends how far you have gone with your mistakes how you revert them. You have the following possibilities.

1. You have done some changes locally and not added the changes to the next commit index.
2. You have done some changes that are locally and added the changes to the index.
3. You have done some changes and committed them locally
4. You have pushed your changes to the remote (Github)


For case 1 and 2 you can simply "checkout" the file again by issuing the command `git checkout filename`. Do some changes, revert the changes by checking the file out again. Note that if you have added the changes to the index area and you want to move these changes back to unindexed area, you can do `git reset filename`.

Now let us recover from case 3. Make some random changes again and commit them - don´t push to Github.
If you do `git log` you can see that the commits you have created have a unique identifier.

One possible way of removing the unwanted commits is to alter the head of your local branch. That is, move the head to a previous commit and discard commits that are in front of that commit id.


If you want to discard un-pushed commits we can use the command `git reset` as follows.

    git reset --hard HEAD~1

This line moves the head of our repo to the commit that is before the current head, that is it moves the head to the previous commit. The number states how many comments we should move from. To learn more about git reset go through the man page for git-reset.

    man git-reset

Do some changes and commit them and remove them using the technique described above.

For case 4. If you have pushed the changes to Github and others have pulled them to their local close - then we have to add revert commits that other developer will pull from Github. We can add a revert commit for selected commits as follows.

    git revert commit-id.

This adds a new revert commit that you need to push to Github.

Try all these possibilities and try to win your way out of troubling and emberising commits and pushes.### Tagging1. All team members agree on a final version of the movie list.2. Create a tag/label for this version and call it "Version 1"
The book has a great chapter on Tagging [http://git-scm.com/book/en/Git-Basics-Tagging](http://git-scm.com/book/en/Git-Basics-Tagging). To solve this part, skim though the pages and apply your knowledge from the book :).## The resultsThe emphasis should be on doing each thing well, and understanding what you are doing. Git has a learning curve. It is not steep, but it might be long. Try to experiment as much as you can. Read the book and don't be afraid to experiment with comments. You should always be able to revert back from your mistakes.