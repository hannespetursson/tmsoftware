**Git Extras**

# Git++
Is this it? No, not really. Git is a versatile and sophisticated version control system. And as with other such tools, it's up to the user to customise it to one's own content. So, where to start?

## Setting up your user
First things first, who are you anyway? You've probably already done something like the following:

    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

But wait, what's this global flag? Git can be configured on three levels, per repository, per user and per computer. When you supply `--global` to the `git config` command you are telling it to change your user's git config. But how do you know what's configured?

    git config --list

That's how! Note that you might see more that just these user and email settings. But what about if you want to just set your credentials or change a setting for a particular git repository? Just skip the global when you are inside git repository.

    git config user.name "Not the same Joe"

To examine your user's git config, there is a file in your home directory `~/.gitconfig` (\*nix systems) or `C:\Users\USERNAME\.gitconfig` (Windows) that you can view and modify. Each repository also has an additional config in the git subfolder `.git/config` (\*nix) `.git\config` (Windows). First Git will check on the repository settings, then the users and finally the system wide git settings.

P.s. now's a **great opportunity** to change your default editor for git commit messages to [vim](http://heather.cs.ucdavis.edu/~matloff/UnixAndC/Editors/ViIntro.html):

    git config --global core.editor vim

## What is this Origin?
If you look a git repository's config file, you will see this `[remote "origin"]` line. It has two settings; url pointing to a remote location and fetch which tells git what branches we should fetch and what should the local copies of those branches be called. There will come a time where you might want to change the fetch definition of a git repository, but now's not the time.

But origin is just a name. You can add as many remote locations as you want, and you can even name it something completely different.

Let's try creating a repo and not use origin as the name of the Github remote.

1. Go to [github.com](github.com), sign in and create a new repository (feel free to be creative in naming) - make sure to skip adding a README file, .gitignore file and a license file to avoid conflicts.
2. Create some random git repo:

    ```
    mkdir kramerica
    cd kramerica
    git init
    echo "Kramerica Industries Inc." > README.md
    git add README.md
    git commit -m "Initial commit"
    git remote add kramer git@github.com:USER/kramerica.git
    git push kramer master
    ```
3. (Optional) You now realise that you need to have this project hosted at [Bitbucket](https://bitbucket.org/) as well. Sign up and login to their services. Create a repository (kramerica) on Bitbucket and do the following in the kramerica folder:

    ```
    git remote add elaine git@bitbucket.org:USER/kramerica.git
    git push -u elaine --all # pushes up the repo and its refs for the first time
    ```

Now you can make changes to the repo, and push to one or both remotes (elaine (Bitbucket) and kramer (Github)). And after you've done this, in the kramerica folder look at the `git config --list`, does it make more sense?

## But wait, that wasn't supposed to go there...
When working with Java projects you will have java source files, you will generate class files. When working with Python you will have python source files, and you will get compiled python files. When working with tests you might generate a test database. You might have sensitive credentials stored in files that shouldn't be distributed with the repository. All kinds of files are not needed in the repository, but when developing and working with a project they will pollute the repository.

### Enter .gitignore
Gitignore is a file that tells Git to ignore certain files within that repo. If a folder or a file name matches an ignore pattern it can't be added to the repo (`git add` will not work). Depending on the project what you would like to have in the `.gitignore` file varies. Try looking at [gitignore.io](https://www.gitignore.io/) and create your own file that you would use for a project like Kramerica.
